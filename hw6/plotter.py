import numpy as np
import matplotlib.pyplot as plt
import collections

def word_count(file_name):
    
    raw_data = open(file_name)
    new_data = raw_data.read()
    clean_data = new_data.replace("\n", "")
	
    d = dict()

    for c in clean_data:
        if c not in d:
            d[c] = 1
        else:
            d[c] += 1

    od = collections.OrderedDict(sorted(d.items()))
    return(od)


if __name__ == '__main__':

    file_path = "./words.txt"
    histogram = word_count(file_path)
    
    x = list(histogram.keys())
    y = list(histogram.values())
    y = np.array(y)
    
    print(word_count(file_path))
    
    plt.bar(x,y)
    plt.plot(x,y, '-o', color = "red")

    plt.xlabel("Character")
    plt.ylabel("Frequency")
    plt.grid()
    plt.show()

    plt.savefig("results.png")
