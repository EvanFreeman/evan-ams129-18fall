def right_justify(stringinput):
    print((70-len(stringinput)) * ' ' + stringinput)


def count_char(s, c):
    print("The total number of that character in the words is:")
    print(s.count(c))


def cumulative_sum(input):
    sum = []
    total = 0
    for i in input:
        total = total + i
        sum.append(total)
    return sum


def check_palindrome(words):
    reverse = ''
    reverse = words[::-1]
    if words == reverse:
        return True
    else:
        return False


def main():
    EvanFreeman = 'EvanFreeman'
    right_justify(EvanFreeman)    # please put your real name

    lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis tellus id mollis scelerisque. In consequat nec tellus lacinia iaculis. Mauris auctor volutpat aliquam. Nulla dignissim arcu placerat tellus pretium, vel venenatis sem porta. Donec interdum tincidunt mi, et convallis velit aliquet eget. Nam id rutrum felis. Fusce vel fermentum justo. Pellentesque faucibus orci at velit vehicula dignissim. Duis faucibus dapibus malesuada. Pellentesque iaculis tristique vestibulum. Sed egestas nisl non augue imperdiet mattis. Nunc vitae purus lectus. Vestibulum mi turpis, volutpat vel odio quis, hendrerit suscipit ligula. Nunc in massa diam. Suspendisse aliquam quam et ex egestas vestibulum vitae id libero. Cras molestie consectetur condimentum.'
    count_char(lorem, 't')

    result = cumulative_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(result)

    words_list = ['noon', 'madam', 'ams129', 'redivider', 'numpy', 'bob', 'racecar', 'youngjun']
    for word in words_list:
        if check_palindrome(word):
            print(word, 'is palindrome!')


if __name__ == '__main__':
    main()
