import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':

    file_path = '../fortran'
    os.chdir(file_path)
    os.system('make && ./euler')
    os.chdir('../python')

    i = [8,16,32,64]
    p = 0
    
    while p < len(i) :
      file = os.path.dirname(os.path.realpath('__file__'))
      function = os.path.join(file, '../fortran/output_%d.txt' %i[p])
      numbers = os.path.abspath(os.path.realpath(function))
      
      solution = np.loadtxt(numbers)
    
      j = 0
      error = 0.
      sol = []

      while j < len(solution) :

        sol.append(-1*(np.sqrt(2*np.log((solution[j][0]**2) + 1) + 4)))

        error += abs(sol[j] - solution[j][1])  

        j+=1

      
      t,y = solution.T

      print(solution)
      figure = plt.figure()
      plt.interactive(False)
      plt.plot(t,y, linestyle = "--", marker = 'o', color = "red")
      plt.plot(t,sol, linestyle = "-", color = "blue")

      plt.xlabel("t axis")
      plt.ylabel("y axis")
      plt.title(error)
      plt.grid()
      plt.show()

      figure.savefig("result_%d.png"%i[p])
      p+=1
