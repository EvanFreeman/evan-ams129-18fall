SUBROUTINE calc_basel(threshold)

  USE param

  IMPLICIT NONE

  int sum = 0.
  int error = 0.
  int i = 0.

  open(unit = 21, file = "results.txt")

  do
  i = i + 1
  sum = sum + (1/(i * i))
  error = ((pi*pi)/6) - sum
  
  IF (error > threshold) THEN
             EXIT

  ELSE IF (MOD(i,1000) == 0) THEN
	!I want a write statement here
             write(21,*) "n = ", i, "error = ", error

  END IF

  end do

  close(21)

END SUBROUTINE calc_basel
