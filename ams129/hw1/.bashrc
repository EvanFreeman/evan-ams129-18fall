#-----------------------------------
# Print current user names
#-----------------------------------
u="$USER"
echo "user name $u"

#-----------------------------------
# Export some paths
#-----------------------------------
export EDITOR="vim"
export IDL_DIR="/usr/local/itt/idl"
export PATH="/usr/local/Cellar/colordiff/1.0.13/bin:/usr/local/bin:$PATH"
export PATH="/usr/local/Cellar/valgrind/3.8.1/bin:$PATH"

.
.
.
.
.
.

#-----------------------------------
# Aliases
#-----------------------------------
# Bash
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx # dark background
alias lls='ls -laghFG'
alias la='ls -lA'
alias ll='ls -lhv'
############################################################
#!/usr/bin/env bash
# ${HOME}/.bashrc: executed by bash(1) for non-login shells.
# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# User Info

export USERNAME="eafreema"
export NICKNAME="Evan"

# Distribute bashrc into smaller, more specific files

source .shells/defaults
source .shells/functions
source .shells/exports
source .shells/alias
source .shells/prompt   # Fancy prompt with time and current working dir
source .shells/git      # Conveniences - Display current branch etc

# Welcome message
echo -ne "Good Morning, $NICKNAME! It's "; date '+%A, %B %-d %Y'
echo -e "And now your moment of Zen:"; fortune
echo
echo "Hardware Information:"
sensors  # Needs: 'sudo apt-get install lm-sensors'
uptime   # Needs: 'sudo apt-get install lsscsi'
lsscsi
free -m
