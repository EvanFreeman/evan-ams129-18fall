def f(x):
    return 2*x**7+4*x**5-2*x**3+3*x+1
def df(x):
    return 14*x**6+20*x**4-6*x**2+3
    
def newtons_method(f, df, initial_guess, threshold, print_res=False):
    count = 0
    temp = 0
    print 'n|', ("\t"), 'x|', ("\t"), 'dx'
    print '-----------------------------------------------'
    while initial_guess > threshold:
        print count, '|', ("\t"), initial_guess, '|', ("\t"), abs(initial_guess - temp)
        count = count + 1
        temp = initial_guess
        initial_guess = initial_guess - f(initial_guess)/df(initial_guess)
    return initial_guess

if __name__ == '__main__':
    initial_guess = 10.
    threshold = 1.E-8
    newtons_method(f, df, initial_guess, threshold, True)
