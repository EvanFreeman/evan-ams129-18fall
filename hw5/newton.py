def newtons(f, df, initial_guess, threshold):
    count = 0
    temp = 0
    print 'n|', ("\t"), 'x|', ("\t"), 'dx'
    print '-----------------------------------------------'
    while initial_guess > threshold:
        print count, '|', ("\t"), initial_guess, '|', ("\t"), abs(initial_guess - temp)
        count = count + 1
        temp = initial_guess
        initial_guess = initial_guess - f(initial_guess)/df(initial_guess)
    return initial_guess
