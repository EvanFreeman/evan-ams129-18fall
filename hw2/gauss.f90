program gauss

  implicit none

  real(kind=8), dimension(3,3) :: A
  real(kind=8), dimension(3) :: b

  integer :: i,j

  ! initialize matrix A and vector b
  A(1,:) = (/2., 3., -1./)
  A(2,:) = (/4., 7., 1./)
  A(3,:) = (/7., 10., -4./)

  b = (/1., 3., 4./)

  ! print augmented matrix
  do i = 1, 3           ! i is row
    print *, A(i,:), "|", b(i)
  end do

  print *, ""    ! print a blank line
  print *, "Gaussian elimination........"
  ! gaussian elimination
  do j = 1, 2           ! j is column
    do i = j+1, 3       ! i is row
      ! STUDENTS: PLEASE FINISH THIS GAUSSIAN ELIMINATION
      B(i) = B(i) - (B(j)*(A(i,j)/A(j,j)))
      A(i,:) = A(i,:) - (A(j,:)*(A(i,j)/A(j,j)))
    end do
  end do

  ! print augmented matrix again
  ! this should be an echelon form (or triangular form)
  print *, "***********************"
  do i = 1, 3
    print *, A(i,:), "|", b(i)
  end do

  print *, ""    ! print a blank line
  print *, "back subs......"

  ! doing back substitution
  do j = 3, 2, -1          ! j is column
    do i = j-1, 1, -1        ! i is row
      ! STUDENTS: PLEASE FINISH THIS BACK SUBSTITUTION
      ! HINT: THIS PART IS PRETTY MUCH SIMILAR WITH GAUSSIAN ELIM. PART.
      B(i) = B(i) - (B(j)*(A(i,j)/A(j,j)))
      A(i,:) = A(i,:) - (A(j,:)*(A(i,j)/A(j,j)))

    end do
  end do

  ! print the results
  print *, "***********************"
  do i = 1, 3
    print *, A(i,:), "|", b(i)
  end do

  print *, "The solutions are:"
  ! STUDENTS: PLEASE CALCULATE A SOLUTION VECTOR, AND PRINT TO THE SCREEN.
  do i = 1, 3
      ! STUDENTS: PLEASE FINISH THIS GAUSSIAN ELIMINATION

      B(i) = B(i)/A(i,i)
      A(i,i) = A(i,i)/A(i,i)
  end do

print *, "x = ", b(1)/A(1,1), ", y = ", b(2)/A(2,2), ", z = ", b(3)/A(3,3)


open (unit = 21, file = "soln.txt")

write(21,*)"The solutions are: "
write(21,*)"x = ", b(1)/A(1,1), ", y = ", b(2)/A(2,2), ", z = ", b(3)/A(3,3)

close(21)


end program gauss
